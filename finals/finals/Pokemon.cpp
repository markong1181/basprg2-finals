#include "Pokemon.h"
#include <iostream>
#include <string>
#include <vector>
#include <time.h>


Pokemon::Pokemon() {
	this->PokemonName = "";
	this->PokemonHp = 0;
	this->PokemonExp = 0;
	this->PokemonDamage = 0;
	this->PokemonLevel = 0;
	this->ExptoNextLevel = 0;
}
Pokemon::Pokemon(string PokemonName, int PokemonHp, int PokemonExp, int PokemonDamage, int PokemonLevel, int ExptoNextLevel)
{
	this->PokemonName = PokemonName;
	this->PokemonHp = PokemonHp;
	this->PokemonExp = PokemonExp;
	this->PokemonDamage = PokemonDamage;
	this->PokemonLevel = PokemonLevel;
	this->ExptoNextLevel = ExptoNextLevel;

}

void Pokemon::DisplayStats()
{
	cout << "Pokemon: " << this->PokemonName << " stats: " << endl;
	cout << "Hp: " << this->PokemonHp << endl;
	cout << "Damage: " << this->PokemonDamage << endl;
}
void Pokemon::Attack(Pokemon* enemy)
{
	cout << this->PokemonName << "attacked" << enemy->PokemonName << endl;
	enemy->PokemonHp -= this->PokemonDamage;
	cout << enemy->PokemonName << " Hp is now: " << enemy->PokemonHp << endl;

}
void pokedex()
{
	vector <Pokemon> Pokedex(15);
	Pokedex[1] = Pokemon("Treecko", 64, 0, 63, 5, 36);
	Pokedex[2] = Pokemon("Torchic", 72, 0, 84, 5, 36);
	Pokedex[3] = Pokemon("Mudkip", 80, 0, 98, 5, 36);
	Pokedex[4] = Pokemon("Poochyena", 35, 0, 55, 1, 20);
	Pokedex[5] = Pokemon("Zigzagoon", 38, 0, 30, 1, 20);
	Pokedex[6] = Pokemon("Wurmple", 45, 0, 45, 1, 20);
	Pokedex[7] = Pokemon("Lotad", 40, 0, 30, 1, 20);
	Pokedex[8] = Pokemon("Taillow", 40, 0, 55, 1, 20);
	Pokedex[9] = Pokemon("Wingull", 40, 0, 30, 1, 20);
	Pokedex[10] = Pokemon("Shroomish", 60, 0, 40, 1, 20);
	Pokedex[11] = Pokemon("Nincada", 31, 0, 45, 1, 20);
	Pokedex[12] = Pokemon("Whismur", 64, 0, 51, 1, 20);
	Pokedex[13] = Pokemon("Magikarp", 20, 0, 10, 1, 20);
	Pokedex[14] = Pokemon("Geodude", 40, 0, 80, 1, 20);
	Pokedex[15] = Pokemon("Zubat", 40, 0, 45, 1, 20);

	
	

		
		
		//HP grows 15% per level
		//Damage grows 10 % per level
		//Exp to Next Level grows 20 % per level
		//Exp to Give grows 20 % per level
}

