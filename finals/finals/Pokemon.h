#pragma once
#include <iostream>
#include <string>
using namespace std;
#ifndef Pokemon_H
class Pokemon
{
public:
	// default constructor
	Pokemon();

// overload constructor
	Pokemon(string PokemonName,int PokemonHp,int PokemonExp,int PokemonDamage,int PokemonLevel,int ExptoNextLevel);
	string PokemonName;
	int PokemonHp;
	int PokemonExp;
	int PokemonDamage;
	int PokemonLevel;
	int ExptoNextLevel;
	void DisplayStats();
	void Attack(Pokemon* enemy);	
};

#endif Pokemon_H



